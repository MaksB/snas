package com.maks.db.dao;



import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.maks.db.model.User;

public class UserDAO {
	private static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	
	public static void add(User user){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		session.close();
	}
	
	public static void update(User user){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(user);
		session.getTransaction().commit();
		session.close();
	}
	
	public static void delete(User user){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(user);
		session.getTransaction().commit();
		session.close();
		
	}
	public static User get(Integer id){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user = session.get(User.class, id);
		session.getTransaction().commit();
		session.close();
		return user;
	}
	public static User getByEmail(String email){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user =(User) session.createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult();
		session.getTransaction().commit();
		session.close();
		return user;
		
	}
	
}
