package com.maks.db.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.maks.db.model.Friends;
import com.maks.db.model.UserFB;

public class FriendsDAO {
	private static SessionFactory sessioFactory =new Configuration().configure().buildSessionFactory();
	
	public static void add(Friends friend){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		session.save(friend);
		session.getTransaction().commit();
		session.close();
	}
	
	public static void delete(Friends friend){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		session.delete(friend);;
		session.getTransaction().commit();
		session.close();
	}
	
	public static void update(Friends friend){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		session.update(friend);
		session.getTransaction().commit();
		session.close();
	}
	
	public static Friends get(Integer id){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		Friends friend= session.get(Friends.class,id);
		session.getTransaction().commit();
		session.close();
		return friend;
	}
	
	public static Friends getByUser(UserFB user, UserFB friend){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		Friends fr= (Friends) session.createCriteria(Friends.class).add(Restrictions.eq("ufb", user)).add(Restrictions.eq("friend", friend)).uniqueResult();
		session.getTransaction().commit();
		session.close();
		return fr;
	}
}
