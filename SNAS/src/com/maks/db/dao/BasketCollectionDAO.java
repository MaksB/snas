package com.maks.db.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.maks.db.model.BasketCollection;
import com.maks.db.model.User;

public class BasketCollectionDAO {
	private static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	
	public static void add(BasketCollection basket){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(basket);
		session.getTransaction().commit();
		session.close();
	}
	
	public static void update(BasketCollection basket){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(basket);
		session.getTransaction().commit();
		session.close();
	}
	
	public static BasketCollection getId(Integer id){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		BasketCollection basket = session.get(BasketCollection.class,id);
		session.getTransaction().commit();
		session.close();
		return basket;
	}
	
	public static void delete(BasketCollection basket){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(basket);
		session.getTransaction().commit();
		session.close();
	}
	
	public static List<BasketCollection> getAllByUser(Integer id){
		List<BasketCollection> list;
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		list = session.createQuery("from BasketCollection where user_id =?").setInteger(0, id).list();
		session.getTransaction().commit();
		session.close();
		return list;
	} 
	
}
