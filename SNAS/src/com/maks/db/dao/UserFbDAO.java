package com.maks.db.dao;

import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.maks.db.model.UserFB;

public class UserFbDAO {

	private static SessionFactory sessioFactory = new Configuration().configure().buildSessionFactory();
	
	public static void add(UserFB user){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		session.close();
	}
	
	public static void delete(UserFB user){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		session.delete(user);;
		session.getTransaction().commit();
		session.close();
	}
	
	public static void update(UserFB user){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		session.update(user);
		session.getTransaction().commit();
		session.close();
	}
	
	public static UserFB get(Integer id){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		UserFB user= session.get(UserFB.class,id);
		session.getTransaction().commit();
		session.close();
		return user;
	}
	
	public static UserFB getByID(String idFb){
		Session session = sessioFactory.openSession();
		session.beginTransaction();
		UserFB user= (UserFB) session.createCriteria(UserFB.class).add(Restrictions.eq("idFb", idFb)).uniqueResult();
		session.getTransaction().commit();
		session.close();
		return user;
	}
}
