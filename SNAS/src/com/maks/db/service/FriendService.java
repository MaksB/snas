package com.maks.db.service;

import com.maks.db.dao.FriendsDAO;
import com.maks.db.model.Friends;
import com.maks.db.model.UserFB;

public class FriendService {
	public static void add(Friends friend){
		System.out.println(friend.getFriend().getId());
		
		if(FriendsDAO.getByUser(friend.getUfb(), friend.getFriend())==null){
			FriendsDAO.add(friend);
		}
		
	}

	public static void delete(Friends friend){
		FriendsDAO.delete(friend);
	}
	
	public static void update(Friends friends){
		FriendsDAO.update(friends);
	}
	
	public static Friends get(Integer id){
		return FriendsDAO.get(id);
	}
	public static Friends getByUser(UserFB user, UserFB friend){
		return FriendsDAO.getByUser(user, friend);
	}
	
}
