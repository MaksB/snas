package com.maks.db.service;

import com.maks.db.dao.UserDAO;
import com.maks.db.model.User;




public class UserService {

	
	public static User get(Integer id) {
		return UserDAO.get(id);
	}

	public static void save(User user) {
		UserDAO.add(user);
	}

	public static void delete(User user) {
		UserDAO.delete(user);
	}

	public static void update(User user) {
		UserDAO.update(user);	
	}
	
	public static User getByEmail(String email){
		return UserDAO.getByEmail(email);
	}

	
}
