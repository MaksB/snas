package com.maks.db.service;

import java.util.List;

import com.maks.db.dao.BasketCollectionDAO;
import com.maks.db.model.BasketCollection;

public class BasketCollectionService {

	public static void add(BasketCollection basket){
		BasketCollectionDAO.add(basket);
	}
	
	public static void delete(BasketCollection basket){
		BasketCollectionDAO.delete(basket);
	}
	
	public static void update(BasketCollection basket){
		BasketCollectionDAO.update(basket);
	}
	public static BasketCollection getId(Integer id){
		return BasketCollectionDAO.getId(id);
	}
	public static List<BasketCollection> getAllByUser(Integer id){
		return BasketCollectionDAO.getAllByUser(id);
	}
}
