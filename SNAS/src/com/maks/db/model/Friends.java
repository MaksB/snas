package com.maks.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Friends {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@OneToOne
	@JoinColumn(name="ufb")
	private UserFB ufb;
	@OneToOne
	@JoinColumn(name="friend")
	private UserFB friend;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UserFB getUfb() {
		return ufb;
	}
	public void setUfb(UserFB ufb) {
		this.ufb = ufb;
	}
	public UserFB getFriend() {
		return friend;
	}
	public void setFriend(UserFB friend) {
		this.friend = friend;
	}
	@Override
	public String toString() {
		return "Friends [id=" + id + ", ufb=" + ufb + ", friend=" + friend
				+ "]";
	}
	
	
}
