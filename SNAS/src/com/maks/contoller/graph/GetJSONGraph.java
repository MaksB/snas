package com.maks.contoller.graph;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.maks.command.Action;
import com.maks.db.model.BasketCollection;
import com.maks.db.service.BasketCollectionService;

public class GetJSONGraph implements Action {

	@Override
	public void execut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		Integer id = Integer.parseInt(request.getParameter("basket"));
		BasketCollection basket = BasketCollectionService.getId(id);
		ParsingFile parse = new ParsingFile();
		JsonObject js = parse.getGraph(basket);
		response.getWriter().print(js);
		System.out.println(js);
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "getGraph";
	}
}
