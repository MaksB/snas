package com.maks.contoller.graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.HashedMap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.maks.db.model.BasketCollection;
import com.maks.db.model.UserFB;
import com.maks.db.service.UserFBService;

public class ParsingFile{

	private Set<String> node;
	private Map<String, String> link;
	private JsonObject jObject;
	public ParsingFile() {
		node = new HashSet<String>();
		link = new HashMap<String, String>();
		jObject =  new JsonObject();
	}
	public JsonObject getGraph(BasketCollection basket){
		File file = new File(basket.getPath()+"selectorPeople.txt");
		 
		 JsonArray nodes = new  JsonArray();
		 JsonArray links = new  JsonArray();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			int id = 0;
			while((line = reader.readLine())!=null){
				String[] mas= line.split(",");
				String first = mas[0];
				if(isNode(first)){
					UserFB user = UserFBService.getByIdFb(first);
					 JsonObject object1 = new JsonObject();
					 object1.addProperty("id", user.getIdFb());
					 object1.addProperty("name", user.getName());
					 object1.addProperty("link", user.getLink());
					 object1.addProperty("photo", user.getPhoto());
					  nodes.add(object1);
					  node.add(first);
				}
				for (int i = 1; i < mas.length; i++) {
					UserFB user; 
					if(isNode(mas[i])){
						user = UserFBService.getByIdFb(mas[i]);
						 JsonObject object1 = new JsonObject();
						 object1.addProperty("id", user.getIdFb());
						 object1.addProperty("name", user.getName());
						 object1.addProperty("link", user.getLink());
						 object1.addProperty("photo", user.getPhoto());
						 nodes.add(object1);
						 node.add(mas[i]);
					}
					if(isLink(first, mas[i])){
						 JsonObject objectLinks = new JsonObject();
						  objectLinks.addProperty("id", id);
						  objectLinks.addProperty("from", first);
						  objectLinks.addProperty("to", mas[i]);
						  links.add(objectLinks);
						  link.put(first, mas[i]);
						  id++;
					}
					
				}
			}
			  jObject.add("nodes", nodes);
			  jObject.add("links", links);
		} catch ( IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jObject;
	}
	
	 private boolean isNode(String id){
		 return  !node.contains(id);
	 }
	 private boolean isLink(String id1, String id2){
		 boolean flag = true;
		 if(link.containsKey(id1)){
			if(link.get(id1).equals(id2)){
				flag = false;
			} 
		 }else if(link.containsKey(id2)){
				if(link.get(id2).equals(id1)){
					flag = false;
				} 
		 }
		 
		 return  flag;
	 }
	 

	
}
