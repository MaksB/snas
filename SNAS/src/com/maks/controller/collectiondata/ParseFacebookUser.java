package com.maks.controller.collectiondata;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.maks.db.model.UserFB;

public class ParseFacebookUser {

	
	private WebDriver driver;
	private WebDriverWait wait;
	private static int count;
	public ParseFacebookUser( WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		
		count = 0;
		
	}
	
	public Set<UserFB> getFriends(String id){
		Set<UserFB> list = new LinkedHashSet<>();
		try{
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().setScriptTimeout(40, TimeUnit.SECONDS);
		System.out.println("parse user " +id);
		if(id.replaceAll("\\d*", "").isEmpty()){
		driver.get("https://www.facebook.com/profile.php?id="+id+"&sk=friends");
		}else{
			driver.get("https://www.facebook.com/"+id+"/friends?pnref=lhc");
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("medley_header_friends")));
		
		for (;;) {
		  
		   
		  try{
			if( !driver.findElement(By.className("mbm")).getText().isEmpty()){
			  break;
			}
		  }catch(NoSuchElementException e){
			  
		  }
			
		   ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,5000)", ""); //y value '400' can be altered
		  try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		}
		
		List<WebElement> frends = driver.findElements(By.className("_698"));
		for (WebElement webElement : frends) {
			UserFB ufb = new UserFB();
			ufb.setIdFb( parseLink( webElement.findElement(By.tagName("a")).getAttribute("href")));
			ufb.setLink(webElement.findElement(By.tagName("a")).getAttribute("href"));
			ufb.setName(webElement.findElement(By.className("fsl")).getText());
			ufb.setPhoto("https://graph.facebook.com/"+getId(webElement.findElement(By.tagName("a")).getAttribute("data-hovercard"))+ "/picture?type=large");
			list.add(ufb);
			
		}
		}catch(Exception e){
			if(count<5){
				count++;
				list = getFriends(id);
				
			}
			count=0;
			
		}
		return list;
	}
	
	public static String parseLink(String href){
		String id = null;
		if(href.contains("id")){
			 Matcher matcher = Pattern.compile("\\d+").matcher(href);
			 if(matcher.find( ))
			  {
			  id = matcher.group();     
			                
			}
		}else{
			 Matcher matcher = Pattern.compile("[\\w|\\s|.]*\\?").matcher(href);
			 if(matcher.find( ))
			  {
			  id = new StringBuffer(matcher.group()).reverse().replace(0, 1, "").reverse().toString();     
			                
			}
		}
		return id;
	}
	
	public String getId(String link){
		String id = null;
		 Matcher matcher = Pattern.compile("id=\\d*").matcher(link);
		 if(matcher.find( ))
		  {
		  id = matcher.group().replace("id=", "");     
		                
		}
		 return id;
	}
	
	
}
