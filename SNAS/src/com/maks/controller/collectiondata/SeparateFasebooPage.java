package com.maks.controller.collectiondata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.maks.db.dao.FriendsDAO;
import com.maks.db.model.BasketCollection;
import com.maks.db.model.Friends;
import com.maks.db.model.User;
import com.maks.db.model.UserFB;
import com.maks.db.service.BasketCollectionService;
import com.maks.db.service.FriendService;
import com.maks.db.service.UserFBService;
import com.maks.db.service.UserService;

public class SeparateFasebooPage {

	private User user;
	private Integer circle;
	private Set<UserFB> friends;
	private WebDriver driver;
	private ParseFacebookUser run;
	private HttpServletRequest request;
	private String fileName;
	private Set<String> unickId;
	private int totalCount;
	public SeparateFasebooPage(User user, Integer circle,
			HttpServletRequest request) {
		this.user = user;
		this.circle = circle;
		this.request = request;
		unickId = new HashSet<String>();
		mainSelector();
	}

	private void mainSelector() {
		FirefoxProfile profile = new FirefoxProfile();
		  profile.setEnableNativeEvents(false);
		  profile.setPreference("permissions.default.image", 2);
		  profile.setPreference("webdriver.load.strategy", "unstable");
		  DesiredCapabilities capabilities = new DesiredCapabilities();
		  capabilities.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "eager");
		  capabilities.setCapability(FirefoxDriver.PROFILE, profile);
		driver = new FirefoxDriver(capabilities);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(40, TimeUnit.SECONDS);
		try {
			driver.get("https://www.facebook.com/");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("email")));
			driver.findElement(By.id("email")).sendKeys("makski-23@mail.ru");
			driver.findElement(By.id("pass")).sendKeys("maksym171993");
			driver.findElement(By.id("u_0_x")).click();
			
	
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("q")));
			if(user.getIdFacebook()==null){
			setIdFacebook(driver.findElement(By.className("_2dpe")).getAttribute("href"));	
			}
			
		} catch (TimeoutException e) {
			driver.close();
			mainSelector();
		}
		System.out.println("again try!!!!!!!!!!!!!");
		 fileName = request.getServletContext().getRealPath("")
				+File.separator+ "UserData" +File.separator+"_" + user.getId()+new Date().getTime()
				+File.separator;
		run = new ParseFacebookUser(driver, wait);
		System.out.println(user);
		friends = run.getFriends(user.getIdFacebook());
		System.out.println(1);
		UserFB ufb = new UserFB();
		ufb.setIdFb(user.getIdFacebook());
		ufb.setName(user.getLastname()+" "+user.getFirstname());
		ufb.setPhoto(user.getUrlPhoto());
		UserFBService.add(ufb);
		writeResaultFile(user.getIdFacebook(), friends, fileName);
		writeTemporalFile(friends, 1);
		unickId.add(user.getIdFacebook());
		readFile();
		
		
		
		BasketCollection basket = new BasketCollection();
		basket.setCircle(circle);
		basket.setDate(new Date());
		basket.setPath(fileName);
		basket.setUser(user);
		basket.setTotalCount(totalCount);
		BasketCollectionService.add(basket);
		driver.close();
		
	}

	private void writeTemporalFile(Set<UserFB> setFriend, Integer iteration) {
		try {
			
			File file = new File(fileName+iteration+".txt");
			Writer write = new FileWriter(file,true);
			
			for (UserFB userFB : setFriend) {
				if(userFB.getIdFb()!=null){
				write.write(userFB.getIdFb() + "\n");
				
				}
			}
			
			write.flush();
			write.close();
			System.out.println(setFriend.size());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(setFriend.size());
	}

	private void writeResaultFile(String id, Set<UserFB> setFriend,
			String fileName) {
		try {
			
			File path = new File(fileName);
			if (!path.exists()) {
				path.mkdirs();
				
			}
			
			File file = new File(fileName+"selectorPeople.txt");
			Writer write = new FileWriter(file,true);
			write.write(id + ",");
			UserFB ufb = UserFBService.getByIdFb(id);
			for (UserFB userFB : setFriend) {
				UserFBService.add(userFB);
				Friends friend = new Friends();
				friend.setUfb(ufb);
				userFB =UserFBService.getByIdFb(userFB.getIdFb());
				friend.setFriend(userFB);
				FriendService.add(friend);
				write.write(userFB.getIdFb() + ",");
			}
			
			write.write("\n");
			write.flush();
			write.close();
			totalCount++;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void readFile() {
		
		for (int i = 1; i < circle; i++) {

			 try(BufferedReader read = new BufferedReader(new FileReader(new File(fileName+i+".txt")))) {
				String id;
				while((id= read.readLine())!=null){
					if(!unickId.contains(id))
					{
		
						unickId.add(id);
						friends= run.getFriends(id);
						writeResaultFile(id, friends, fileName);
						if(i==circle)
							continue;
							writeTemporalFile(friends, i+1);
					}
					
				
					
					
				}
				
			} catch ( IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	

	}
	
	private void setIdFacebook(String link){
		String id= ParseFacebookUser.parseLink(link);
		user.setIdFacebook(id);
		UserService.update(user);
	}
}
