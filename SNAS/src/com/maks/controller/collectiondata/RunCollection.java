package com.maks.controller.collectiondata;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.maks.command.Action;
import com.maks.db.model.User;

public class RunCollection implements Action {

	@Override
	public void execut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		long start = System.currentTimeMillis();
		Integer circle = Integer.parseInt(request.getParameter("circle"));
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		SeparateFasebooPage parse = new SeparateFasebooPage(user,circle,request);
		System.out.println("time collection: "+(System.currentTimeMillis()-start)/1000);
		
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "run";
	}
}
