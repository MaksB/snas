package com.maks.controller.collectiondata;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.maks.command.Action;
import com.maks.db.model.BasketCollection;
import com.maks.db.model.User;
import com.maks.db.service.BasketCollectionService;

public class CollectionIndex implements Action {

	@Override
	public void execut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		
		List<BasketCollection> list = BasketCollectionService.getAllByUser(user.getId());
		request.setAttribute("basketList", list);
		
		request.getRequestDispatcher("WEB-INF/page/collection.jsp").forward(request, response);
		
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "index";
	}
}
