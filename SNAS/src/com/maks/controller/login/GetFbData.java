package com.maks.controller.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;





import java.util.zip.DataFormatException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;









import com.maks.command.Action;
import com.maks.db.dao.UserDAO;
import com.maks.db.model.User;
import com.maks.db.service.UserService;

public class GetFbData implements Action {

	public static final String FB_APP_ID = "1144593282235270";
	public static final String FB_APP_SECRET = "a742aabcab23defa932b7e90073b73ca";
	public static final String REDIRECT_URI = "http://localhost:8080/SNAS/login?action=access";
	

	static String accessToken = "";
	private String code="";
	
	@Override
	public void execut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		code = request.getParameter("code");
		
		if (code == null || code.equals("")) {
			response.sendError(404);
			return;
		}
		
		String accessToken = getAccessToken(code);
		FBGraph fbGraph = new FBGraph(accessToken);
		String graph = fbGraph.getFBGraph();
		Map<String, String> fbProfileData = fbGraph.getGraphData(graph);
			
			String email =fbProfileData.get("email");
			User user = UserService.getByEmail(email);
			if(user==null){
				user = new User();
				user.setEmail(email);
				user.setFirstname(fbProfileData.get("first_name"));
				user.setLastname(fbProfileData.get("last_name"));
				user.setPhoto(fbProfileData.get("photo"));
				user.setAge(Integer.parseInt(fbProfileData.get("age")));
				user.setGender(fbProfileData.get("gender"));
				user.setLocation(fbProfileData.get("location"));
				SimpleDateFormat sd = new SimpleDateFormat("mm/dd/yyyy");
				
					try {
					
						user.setBirthday(sd.parse(fbProfileData.get("birthday")));
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				UserService.save(user);
			}
			
			request.getSession().setAttribute("user", user);
		
			request.getRequestDispatcher("/UserServlet?action=Upage").forward(request, response);
			
		
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "access";
	}
	public String getFBGraphUrl(String code) {
		String fbGraphUrl = "";
		try {
			fbGraphUrl = "https://graph.facebook.com/oauth/access_token?"
					+ "client_id=" + FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(REDIRECT_URI, "UTF-8")
					+ "&client_secret=" + FB_APP_SECRET + "&code=" + code;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return fbGraphUrl;
	}

	public String getAccessToken(String code) {
		if ("".equals(accessToken)) {
			URL fbGraphURL;
			try {
				fbGraphURL = new URL(getFBGraphUrl(code));
			} catch (MalformedURLException e) {
				e.printStackTrace();
				throw new RuntimeException("Invalid code received " + e);
			}
			URLConnection fbConnection;
			StringBuffer b = null;
			try {
				fbConnection = fbGraphURL.openConnection();
				BufferedReader in;
				in = new BufferedReader(new InputStreamReader(
						fbConnection.getInputStream()));
				String inputLine;
				b = new StringBuffer();
				while ((inputLine = in.readLine()) != null)
					b.append(inputLine + "\n");
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Unable to connect with Facebook "
						+ e);
			}

			accessToken = b.toString();
			if (accessToken.startsWith("{")) {
				throw new RuntimeException("ERROR: Access Token Invalid: "
						+ accessToken);
			}
		}
		return accessToken;
	}

}
