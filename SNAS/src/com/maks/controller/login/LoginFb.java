package com.maks.controller.login;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.maks.command.Action;

public class LoginFb implements Action {
	
	public static final String FB_APP_ID = "1144593282235270";
	public static final String FB_APP_SECRET = "a742aabcab23defa932b7e90073b73ca";
	public static final String REDIRECT_URI = "http://localhost:8080/SNAS/login?action=access";

	static String accessToken = "";
	
	
	@Override
	public void execut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect(getFBAuthUrl());
		return;
		
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "login";
	}
	

	public String getFBAuthUrl() {
		String fbLoginUrl = "";
		try {
			fbLoginUrl = "http://www.facebook.com/dialog/oauth?" + "client_id="
					+ FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(REDIRECT_URI, "UTF-8")
					+ "&scope=email";
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return fbLoginUrl;
	}
}
