package com.maks.controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.maks.command.Action;

public class UserPage implements Action {

	@Override
	public void execut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/page/UserPage.jsp").forward(request, response);
	
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Upage";
	}
}
