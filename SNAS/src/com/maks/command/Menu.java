package com.maks.command;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Menu {

	private Map<String, Action> map;
	private String action;
	private Action task;

	public Menu(Action... action) {

		map = new HashMap<String, Action>();

		for (Action item : action) {
			map.put(item.getName(), item);
		}

	}

	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		action = request.getParameter("action");

		task = map.get(action);
		try{
		task.execut(request, response);
		}catch(NullPointerException e){
			response.sendError(404);
			return;
		}

	}

}
