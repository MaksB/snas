<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="head.jsp"/>

<body data-spy="scroll">

	<jsp:include page="menu.jsp"/>
	<!-- Section: intro -->
 
     <section id="about" class="home-section text-center bg-gray ">
 <div class="container ">
	<div class="row">

		<div class="col-md-3">
		
		</div>
		
		<div class="col-md-9">
		
				<div class="col-md-4">
					<form action='<c:url value="collection?action=run"/>' class="form-horizontal" role="form" 
						method="post">
						<div class="form-group">
								<div class="col-sm-10">
									<select class="form-control"  name="circle">
										<option selected  value="0"></option>
										
			<option value="1">First circle</option>
			<option value="2">Second circle</option>
			<option value="3">Third circle</option>
			<option value="4">Fourth circle</option>
			<option value="5">Fifth circle</option>
		

									</select>
								</div>
							</div>
						<div class="form-group">
							<div class="col-sm-offset-0 col-sm-10">
								<button type="submit" class="btn btn-skin scroll">Run</button>
							</div>
						</div>
					</form>
				
				
				</div>
				<div class="col-md-8 normalf">
				
				<h1>History</h1>
				<table class="table">
				<thead>
				<tr>
				<td>Circle</td>
				<td>Total count</td>
				<td>Time</td>
				</tr>
				
				
				</thead>
				<tbody>
				
				<c:forEach items="${basketList}" var="basket">
				<tr>
				<td>${basket.getCircle()}</td>
				<td>${basket.getTotalCount()}</td>
				<td>${basket.getDate()}</td>
				</tr>
				</c:forEach> 
				</tbody>
			</table>
				</div>
			</div>
		</div>
		</div>
		</section>	
	


	
	
<jsp:include page="footer.jsp"/>
<jsp:include page="script.jsp"/>

</body>

</html>
