<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="head.jsp"/>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<body data-spy="scroll">

	<jsp:include page="menu.jsp"/>
	<!-- Section: intro -->
 
     <section id="about" class="home-section text-center bg-gray ">
 <div class="container ">
	<div class="row">

		<div class="col-md-3">
		
		</div>
		
		<div class="col-md-9 normalf">
		
				<div class="col-md-4">
					<form id="form1" action='<c:url value="GraphServlet?action=getGraph"/>' class="form-horizontal" role="form" 
						method="post">
						<div class="form-group">
								<div class="col-sm-10">
									<select class="form-control"  name="basket">
									
										<c:forEach items="${basketList}" var="basket">
										<option value="${basket.getId()}">${basket.getCircle()} circle ${basket.getDate()}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						<div class="form-group">
							<div class="col-sm-offset-0 col-sm-10">
								<button id="show" type="submit" class="btn btn-skin scroll">Show</button>
							</div>
						</div>
					</form>
				
				
				</div>
				<div class="col-md-8 ">
				
				
				
				</div>
			</div>
		</div>
		</div>
		</section>	
	
		<script src="https://cdn.zoomcharts-cloud.com/1/latest/zoomcharts.js"  type="text/javascript"></script> 

	<div id="netchart"></div> 
 			<script>
   



var form = $('#form1');
form.submit(function() {

	request = $.ajax({
		type : 'get',
		url : form.attr('action'),

		data : new FormData(this),
		processData : false,
		contentType : false,

		data : form.serialize(),
		success : function(text) {
			var nc = new NetChart({
			    container: document.getElementById("netchart"),
			    area: { height: 550 },
			    data: { preloaded:text
			    },
			    navigation: {
			        mode: "showall"
			    },
			    style: {
			        link: { fillColor: "limegreen" },
			        node: { imageCropping: true },
			        nodeStyleFunction: nodeStyle
			    },
			    layout:{mode:"fixed"},
			    interaction: { selection: { lockNodesOnMove: false } }
			});
		},
		error : function() {
			
			
		}
	});

	return false;
});

function nodeStyle(node) {
    node.image = node.data.photo;
    node.label = node.data.name;
}
</script>

	
	
<jsp:include page="footer.jsp"/>
<jsp:include page="script.jsp"/>
 
</body>

</html>
