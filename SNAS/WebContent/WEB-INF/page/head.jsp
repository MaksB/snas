<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ninestars bootstrap 3 one page template</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="resources/css/nivo-lightbox.css" rel="stylesheet" />
	<link href="resources/css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="resources/css/animate.css" rel="stylesheet" />
    <!-- Squad theme CSS -->
    <link href="resources/css/style.css" rel="stylesheet">
	<link href="resources/color/default.css" rel="stylesheet">

</head>