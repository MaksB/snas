<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="container">
			<ul id="gn-menu" class="gn-menu-main">
				<li class="gn-trigger">
				<c:if test="${user!=null }">
				<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
				</c:if>
					
					<nav class="gn-menu-wrapper">
						<div class="gn-scroller">
							<ul class="gn-menu">
								<li>
									<a href='<c:url value="/UserServlet?action=Upage"/>' class="gn-icon  glyphicon-user" >My page</a>
								</li>
								<li>
								<a href='<c:url value="/collection?action=index"/>' class="gn-icon gn-icon-download">Collection data</a> 
								</li>
								<li><a href='' class="gn-icon gn-icon-cog">Service</a></li>
								<li><a href='<c:url value="/GraphServlet?action=index"/>' class="gn-icon gn-icon-help">Graph</a></li>
								<li>
									<a href="#contact" class="gn-icon gn-icon-archive">Contact</a>
								</li>
								
							</ul>
						</div><!-- /gn-scroller -->
					</nav>
				</li>
				<li><a href='<c:url value="/index"/>'>Social network analysis system</a></li>
				<li><ul class="company-social">
							<c:choose>
							<c:when test="${user!=null}">
							<li><a href='<c:url value="/exit" />'>Exit</a></li>
							</c:when>
						<c:otherwise>
							 <li class="social-facebook"><a href='<c:url value="/login?action=login" />'><i class="fa fa-facebook"></i></a></li>
							</c:otherwise>
							</c:choose>
							
							
							
                            
                           
                        </ul>	</li>
			</ul>
	</div>
