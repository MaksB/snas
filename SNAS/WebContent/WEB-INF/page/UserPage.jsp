<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="head.jsp"/>

<body data-spy="scroll">

	<jsp:include page="menu.jsp"/>

	<!-- Section: intro -->
 
     <section id="about" class="home-section text-center bg-gray">
 	<div class="container">
	<div class="row">

		<div class="col-md-3">
		
		</div>
		
		<div class="col-md-9">
		
				<div class="col-md-4">
				
				 <div class="avatar"><img src="${user.getPhoto()}" alt="" class="img-responsive" /></div>
				
				</div>
				<div class="col-md-8">
				<div class="description-user">
				<p>First name: ${user.getFirstname()}</p>
				<p>Last name: ${user.getLastname()}</p>
				<p>Email: ${user.getEmail()}</p>
				<p>Location: ${user.getLocation()}</p>
				<p>Birthday: ${user.getBirthday()}</p>
				</div>
		
			
				</div>
			</div>
		</div>
		</div>
		</section>	
	


	
	
<jsp:include page="footer.jsp"/>
<jsp:include page="script.jsp"/>

</body>

</html>
